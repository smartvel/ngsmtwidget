
(function (root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['angular'], factory);
  } else if (typeof module !== 'undefined' && typeof module.exports === 'object') {
    // CommonJS support (for us webpack/browserify/ComponentJS folks)
    module.exports = factory(require('angular'));
  } else {
    // in the case of no module loading system
    // then don't worry about creating a global
    // variable like you would in normal UMD.
    // It's not really helpful... Just call your factory
    return factory(root.angular);
  }
}(this, function (angular) {
  'use strict';
  // create your angular module and do stuff
  var moduleName = 'smtwidget';
  var mod = angular.module(moduleName, []);
  mod.directive('smtplanner', function tPlanerDirective( $window ){
    var mEl = null

    return {
      restrict: 'E',
      link: link,
      template: '<SmartvelComponent></SmartvelComponent>',
      scope: {
        props: '='
      }
    };

    function link( scope, element ) {
      mEl = element[0].getElementsByTagName('SmartvelComponent')[0]
      scope.$watch( 'props', renderReactElement );
      scope.$on( '$destroy', unmountReactElement );
      document.addEventListener('widgetLoaded', initializeWidget, true)
      if(!!$window.SmtBoot && !$window.SmtBoot.initialized && !!$window.SMTWidget){
        var event = new CustomEvent('SMTEngage');
        document.dispatchEvent(event);
      }
      if(!$window.SmtBoot){
        var $script = document.createElement('script');
        //$script.src = '/dist/boot.min.js';
        $script.src = 'https://cdn-qa.smartvel.com/scripts/boot.min.js';
        document.body.appendChild($script);
      }
    }

    function setProps(props){
      if (!!props){
        var ks = Object.keys(props)
        if(!!mEl.dataset){
          ks.forEach(function(el){
            mEl.dataset[el] = props[el]
          })
        }
      }
    }

    function unmountReactElement(){
      console && console.log('Unmount')
      $window.SmtBoot.initialized = false
    }

    function renderReactElement(np){
      setProps(np)
      if(!!$window.SMTWidget){
        $window.SMTWidget.updateDataset(np);
        var event = new CustomEvent('SMTEngage');
        document.dispatchEvent(event);
      }
    }

    function initializeWidget(someprops){
      !!console && console.log('Init Fn', someprops)
      if(!!$window.SmtBoot && !$window.SmtBoot.initialized){
        $window.SmtBoot.boot();
      }
    }
  })
  var event = new Event('SMtNgUtilsLoaded');
  !!document && document.dispatchEvent(event);
}))
